package somepackage;

import io.vavr.Tuple2;
import io.vavr.collection.Stream;
import io.vavr.collection.Traversable;
import io.vavr.control.Either;
import io.vavr.control.Option;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Runner {

    private static final Map<String, SomeResult> mockedResponses = new HashMap<>();
    private static final String FIRST_RESULT = "first";

    static {
        String token = addFirstResult();
        for (int i = 0; i < 10; i++) {
            token = addNextResult(token);
        }
        addErrorResult(token);
    }

    private static void addErrorResult(String token) {
        mockedResponses.put(token, SomeResult.bad());
    }

    private static void addLastResult(String token) {
        mockedResponses.put(token, SomeResult.last());
    }

    private static String addNextResult(String token) {
        final String nextToken = UUID.randomUUID().toString();
        mockedResponses.put(token, SomeResult.of(nextToken));
        return nextToken;
    }

    private static String addFirstResult() {
        final String nextToken = UUID.randomUUID().toString();
        mockedResponses.put(FIRST_RESULT, SomeResult.of(nextToken));
        return nextToken;
    }

    public void run() {
        AtomicBoolean shouldIterateNext = new AtomicBoolean(true);
        final Either<SomeWrapper, SomeResult> seed = getResponse(FIRST_RESULT);
        //atomic boolean to get n + 1
        final Stream<Either<SomeWrapper, SomeResult>> afterUnfold = Stream.unfold(seed, page ->
                page.isLeft() || page.get().isLast()
                        ? Option.none()
                        : Option.of(new Tuple2<>(getResponse(page.get().getNextToken()), page)));
        final SomeWrapper fold = afterUnfold
                .takeRightUntil(Either::isLeft)
                .map(errorOrPage -> errorOrPage.flatMap(e -> Either.right(e.getInsideList())))
                .transform(Either::sequence)
                .fold(Traversable::get, succ -> SomeWrapper.of(succ.flatMap(ArrayList::new).toJavaList()));
        System.out.println(String.join(" ", fold.getInsideList()));




                /*.flatMap(e -> e.flatMap(ArrayList::new).toStream().transform(Either::right))
                .swap()
                .map(Traversable::get)
                .swap();*/
        //.partition(Either::isLeft)
        //.apply((left, right) -> left.isEmpty() ? SomeWrapper.of(right.flatMap(Either::get).toJavaList()) : left.head().swap().get());

        /*Either.sequence(collect)
                .fold(error -> error, success -> SomeWrapper.of(success));*/

        //.map(errorOrPage -> errorOrPage.fold(error -> error, page -> SomeWrapper.of(page)));
    }

    public Either<SomeWrapper, SomeResult> getResponse(String token) {
        final SomeResult someResult = mockedResponses.get(token);
        if (someResult.isBad()) {
            return Either.left(SomeWrapper.bad());
        }
        return Either.right(someResult);
    }

    public SomeResult returningBad() {
        return SomeResult.bad();
    }
}

package somepackage;

import java.util.Arrays;
import java.util.List;

public class SomeWrapper {
    private static final SomeWrapper BAD_WRAPPER = new SomeWrapper();
    private final List<String> insideList;

    public SomeWrapper(List<String> insideList) {
        this.insideList = insideList;
    }

    public static SomeWrapper of(List<String> insideList) {
        return new SomeWrapper(insideList);
    }

    public List<String> getInsideList() {
        return insideList;
    }

    private SomeWrapper() {
        this.insideList = Arrays.asList("error list");
    }

    public boolean isBad() {
        return BAD_WRAPPER == this;
    }

    public static SomeWrapper bad() {
        return BAD_WRAPPER;
    }
}

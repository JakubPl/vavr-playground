package somepackage;

import java.util.Arrays;
import java.util.List;

public class SomeResult {

    private static final SomeResult BAD_RESULT = new SomeResult();
    private static final SomeResult VERY_BAD_RESULT = new SomeResult();
    private static final SomeResult LAST = new SomeResult();

    private final String nextToken;

    private List<String> insideList = Arrays.asList("hello", "world");


    private SomeResult() {
        this.nextToken = null;
    }

    private SomeResult(String nextToken) {
        this.nextToken = nextToken;
    }

    public static SomeResult of(String nextToken) {
        return new SomeResult(nextToken);
    }

    public String getNextToken() {
        return nextToken;
    }

    public List<String> getInsideList() {
        return insideList;
    }

    public boolean isLast() {
        return this == LAST;
    }

    public boolean isBad() {
        return this == BAD_RESULT;
    }

    public static SomeResult bad() {
        return BAD_RESULT;
    }

    public static SomeResult last() {
        return LAST;
    }

    public static SomeResult veryBad() {
        return VERY_BAD_RESULT;
    }
}
